import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";

//Paths
import { DEMO, HOME } from "./_Utils/Routes";

//Components
import ComponentOne from "./ComponentTypeOne/FileNameOne";

export default class RouterComponent extends Component {
  render() {
    return (
      <React.Fragment>
        <Switch>
          <Route exact path={HOME} component={ComponentOne} />

        </Switch>
      </React.Fragment>
    );
  }
}
