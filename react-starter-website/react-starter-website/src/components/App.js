import React, { Component } from 'react';
import FileNameOne from '../components/ComponentTypeOne/FileNameOne'
import { ZoomMtg } from '@zoomus/websdk';
/***/
// import * as base64JS from 'js-base64';
// import * as hmacSha256 from 'crypto-js/hmac-sha256';
// import * as encBase64 from 'crypto-js/enc-base64';



export default class App extends Component {
    constructor() {
        super();
        this.state = {
            openZoom: false,
        }
    }
    // generateSignature(data) {
    //     let signature = '';
    //     // Prevent time sync issue between client signature generation and zoom
    //     const ts = new Date().getTime() - 30000;
    //     try {
    //         const msg = base64JS.Base64.encode(data.apiKey + data.meetingNumber + ts + data.role);
    //         const hash = hmacSha256.default(msg, data.apiSecret);
    //         signature = base64JS.Base64.encodeURI(`${data.apiKey}.${data.meetingNumber}.${ts}.${data.role}.${encBase64.stringify(hash)}`);
    //     } catch (e) {
    //         console.log('error')
    //     }
    //     return signature;
    // }
    componentDidMount() {
        /******************** */

        // ZoomMtg.setZoomJSLib('https://source.zoom.us/1.7.5/lib', '/av');
        ZoomMtg.preLoadWasm();
        ZoomMtg.prepareJssdk();
        const meetConfig = {
            meetingNumber: 123456789,
            userName: "Dev",
            passWord: '',
            leaveUrl: 'http://localhost:3000/'
        }
        ZoomMtg.generateSignature({
            meetingNumber: meetConfig.meetingNumber,
            apiKey: 'lZYy9oF1QCe6jKm0hHDX0Q',
            apiSecret: 'UvBjoZBS2oK53vviWiv8XbToapRoNzePLErO',
            role: 1,
            success(res) {
                console.log('signature', res.result)
                ZoomMtg.init({
                    leaveUrl: meetConfig.leaveUrl,
                    // disableCallOut: true,
                    // disableRecord: true,
                    // isSupportQA: false,
                    // isSupportCC: false,
                    // showPureSharingContent: false,
                    // screenShare: false,
                    // disableJoinAudio: false,
                    // audioPanelAlwaysOpen: false,
                    // isSupportChat: true,
                    // videoDrag: false,
                    // disableInvite: true,
                    success() {
                        ZoomMtg.join(
                            {
                                meetingNumber: meetConfig.meetingNumber,
                                userName: meetConfig.userName,
                                userEmail: 'devenghadge13@gmail.com',
                                passWord: meetConfig.passWord,
                                apiKey: 'lZYy9oF1QCe6jKm0hHDX0Q',

                                signature: res.result,

                                role: 1,
                                success() {
                                    console.log('join meeting success')
                                },
                                error(res) {
                                    console.log('Error generating signature')
                                    console.log(res)
                                }
                            }
                        )
                    },
                    error(res) {
                        console.log(res)
                    }
                })
            }
        })

        /******************* */
        // console.log('checkSystemRequirements');
        // console.log(JSON.stringify(ZoomMtg.checkSystemRequirements()));

        // it's option if you want to change the WebSDK dependency link resources. setZoomJSLib must be run at first
        // if (!china) ZoomMtg.setZoomJSLib('https://source.zoom.us/2.2.0/lib', '/av'); // CDN version default
        // else ZoomMtg.setZoomJSLib('https://jssdk.zoomus.cn/2.2.0/lib', '/av'); // china cdn option
        // ZoomMtg.setZoomJSLib('http://localhost:9999/node_modules/@zoomus/websdk/dist/lib', '/av'); // Local version default, Angular Project change to use cdn version

        // ZoomMtg.preLoadWasm();
        // ZoomMtg.prepareJssdk();

        // ZoomMtg.setZoomJSLib('https://source.zoom.us/1.9.6/lib', '/av');
        // ZoomMtg.init({
        //     debug: true, //optional
        //     leaveUrl: 'http://www.zoom.us', //required
        //     webEndpoint: 'PSO web domain', // PSO option
        //     showMeetingHeader: false, //option
        //     disableInvite: false, //optional
        //     disableCallOut: false, //optional
        //     disableRecord: false, //optional
        //     disableJoinAudio: false, //optional
        //     audioPanelAlwaysOpen: true, //optional
        //     showPureSharingContent: false, //optional
        //     isSupportAV: true, //optional,
        //     isSupportChat: true, //optional,
        //     isSupportQA: true, //optional,
        //     isSupportPolling: true, //optional
        //     isSupportBreakout: true, //optional
        //     isSupportCC: true, //optional,
        //     screenShare: true, //optional,
        //     rwcBackup: '', //optional,
        //     videoDrag: true, //optional,
        //     sharingMode: 'both', //optional,
        //     videoHeader: true, //optional,
        //     isLockBottom: true, // optional,
        //     isSupportNonverbal: true, // optional,
        //     isShowJoiningErrorDialog: true, // optional,
        //     disablePreview: false, // optional
        //     disableCORP: true, // optional
        //     inviteUrlFormat: '', // optional
        //     loginWindow: {  // optional,
        //         width: 400,
        //         height: 380
        //     },
        //     meetingInfo: [ // optional
        //         'topic',
        //         'host',
        //         'mn',
        //         'pwd',
        //         'telPwd',
        //         'invite',
        //         'participant',
        //         'dc',
        //         'enctype',
        //         'report'
        //     ],
        //     disableVoIP: false, // optional
        //     disableReport: false, // optional
        // });

        // ZoomMtg.join({
        //     meetingNumber: 123456789,
        //     userName: 'Dev G',
        //     userEmail: 'devenghadge13@gmail.com',
        //     passWord: '',
        //     apiKey: 'lZYy9oF1QCe6jKm0hHDX0Q',
        //     signature: this.generateSignature,
        //     success: function (res) { console.log(res) },
        //     error: function (res) { console.log(res) }
        // });
    }
    render() {
        return (
            <>
                {!this.state.openZoom &&
                    <div>
                        <button className="btnStartMeeting" onClick={() => {
                            this.setState({
                                openZoom: true
                            })
                        }}>
                            Start Zoom Meeting
                        </button>

                    </div>}
                {
                    this.state.openZoom && <FileNameOne />

                }
            </>
        )
    }
}
