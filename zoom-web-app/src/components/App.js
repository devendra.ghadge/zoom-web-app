import React, { Component } from 'react';
import { Router } from 'react-router-dom';
import Zoom from './ComponentTypeOne/Zoom';
import RouterComponent from './RouterComponent';
import HistoryObject from './_Utils/History';

export default class App extends Component {
    constructor() {
        super()
        this.state = {
            openZoom: false,
        }
    }
    render() {
        return (
            <Router history={HistoryObject}>
                <RouterComponent />
            </Router>
            // <>
            //     {!this.state.openZoom &&
            //         <div>
            //             <button className="btnStartMeeting" onClick={() => {
            //                 this.setState({
            //                     openZoom: true
            //                 })
            //             }}>
            //                 Start Zoom Meeting
            //             </button>

            //         </div>}
            //     {
            //         this.state.openZoom && <Zoom />

            //     }
            // </>
        )
    }
}
