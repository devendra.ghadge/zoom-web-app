import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";

//Paths
import { HOME } from "./_Utils/Routes";

//Components
import Zoom from "./ComponentTypeOne/Zoom";


export default class RouterComponent extends Component {
  render() {
    return (
      <React.Fragment>
        <Switch>
          <Route exact path={HOME} component={Zoom} />
        </Switch>
      </React.Fragment>
    );
  }
}
