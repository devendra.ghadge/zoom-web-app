import axios from 'axios';
import React, { Component } from 'react';

// const [username, setUsername] = useState("");
let username = "dev";
class Zoom extends Component {

    zoomMeeting = () => {
        const data = {
            email: "devenghadge13@gmail.com",
        };
        console.log(username);
        axios
            .post(`http://localhost:3444/meeting`, data)
            .then((response) => {
                let URL =
                    response.data.join_url.replaceAll(
                        "https://us04web.zoom.us/j/",
                        "http://localhost:9999/?"
                    ) + `?role=1?name=${username}`;
                console.log(URL);
                window.location.replace(`${URL}`);
            })
            .catch((err) => console.error(err));
    };
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    {/* <Student /> */}
                    <h1>Zoom Meeting</h1>
                    <div className="card">
                        <div>
                            {/* Name&nbsp;&nbsp; */}
                            <input
                                type="text"
                                name="name"
                                style={{
                                    width: "300px",
                                    borderRadius: "5px",
                                    padding: "8px 12px",
                                    fontSize: "18px",
                                }}
                            // value={username}
                            // onChange={(e) => setUsername(e.target.value)}
                            />
                        </div>

                        <div className="row" style={{ margin: "10px" }}>
                            <div className="column">
                                <div
                                    className="row"
                                    style={{ margin: "10px", marginTop: "120px" }}
                                >
                                    <button
                                        className="btn btn-info"
                                        style={{
                                            width: "290px",
                                            height: "80px",
                                            fontSize: "20px",
                                            fontFamily: "cursive",
                                        }}
                                        onClick={this.zoomMeeting}
                                    >
                                        Create Meeting
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </header>
            </div>
        )
    }
}

export default Zoom
