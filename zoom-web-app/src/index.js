import React from "react";
import ReactDOM from "react-dom";

//style
import "./style/index.css";

//components
import App from "./components/App";
import ZoomVideo from "@zoom/videosdk";
import ZoomContext from "@zoom/videosdk";


ReactDOM.render(
    <App />
    ,
    document.getElementById("root")
);
